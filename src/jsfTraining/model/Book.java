package jsfTraining.model;

import java.io.Serializable;

public class Book implements Serializable{

    public Book(String title) {
        this.title = title;
    }

    public String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
