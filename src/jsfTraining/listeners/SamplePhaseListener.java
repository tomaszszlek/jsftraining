package jsfTraining.listeners;

import java.util.logging.Logger;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class SamplePhaseListener implements PhaseListener{
	private static final long serialVersionUID = 1L;
	
	private final static Logger log = Logger.getLogger(SamplePhaseListener.class.getName()); 
	@Override
	public void afterPhase(PhaseEvent phase) {
		log.info("END Phase: "+ phase.getPhaseId());
		
	}

	@Override
	public void beforePhase(PhaseEvent phase) {
		log.info("START Phase: "+ phase.getPhaseId());
		
	}

	@Override
	public PhaseId getPhaseId() {
		// TODO Auto-generated method stub
		return PhaseId.ANY_PHASE;
	}

}
