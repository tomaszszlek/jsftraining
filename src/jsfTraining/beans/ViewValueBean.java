package jsfTraining.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import jsfTraining.model.Book;

@ManagedBean
@ViewScoped
public class ViewValueBean implements Serializable {

    private boolean display = true;

    private String label = "Some text";
    private String label1 = "Label 1";
    private String label2 = "Label 2";
    private String label3 = "Label 3";
    private String label4 = "Label 4";

    @PostConstruct
    public void init(){
        books.add(new Book("Book 1"));
        books.add(new Book("Book 2"));
        books.add(new Book("Book 3"));
    }

    private List<Book> books = new ArrayList<Book>();
    private Book chosenBook;

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel1(String label1) {
        this.label1 = label1;
    }

    public String getLabel1() {
        return label1;
    }

    public void setLabel2(String label2) {
        this.label2 = label2;
    }

    public String getLabel2() {
        return label2;
    }

    public void setLabel3(String label3) {
        this.label3 = label3;
    }

    public String getLabel3() {
        return label3;
    }

    public void setLabel4(String label4) {
        this.label4 = label4;
    }

    public String getLabel4() {
        return label4;
    }

    public Book getChosenBook() {
        return chosenBook;
    }

    public void setChosenBook(Book chosenBook) {
        this.chosenBook = chosenBook;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
