package jsfTraining.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class SimpleController {

	@ManagedProperty(value="#{simpleValueBean}")
	private SimpleValueBean valueBean;

	@ManagedProperty(value="#{viewValueBean}")
	private ViewValueBean viewValueBean;
	
	public SimpleValueBean getValueBean() {
		return valueBean;
	}
	
	public void setValueBean(SimpleValueBean valueBean) {
		this.valueBean = valueBean;
	}

	public ViewValueBean getViewValueBean() {
		return viewValueBean;
	}

	public void setViewValueBean(ViewValueBean viewValueBean) {
		this.viewValueBean = viewValueBean;
	}

	public String clearValues() {
		getValueBean().setLabel("");
		return null;
	}

    public String doNotDisplay() {
        getViewValueBean().setDisplay(false);
		getViewValueBean().setLabel(Double.toString(Math.random()));
        return null;
    }

}
